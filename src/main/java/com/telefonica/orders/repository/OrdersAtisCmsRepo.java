package com.telefonica.orders.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.orders.entity.HistoricalOrdersAldm;
import com.telefonica.orders.entity.OrdersAtisCms;

@Repository
public interface OrdersAtisCmsRepo extends JpaRepository<OrdersAtisCms, Serializable> {

	@Query("select u from OrdersAtisCms u where u.phoneNumber = ?1 and u.orderDate between ?2 and ?3")
	public abstract List<OrdersAtisCms> findOrdersAtisAmdocs(String publicId, Date startDate, Date endDate);

	@Query("select u from OrdersAtisCms u where u.serviceTvCode = ?1 and u.orderDate between ?2 and ?3")
	public abstract List<OrdersAtisCms> findOrdersCMS(String publicId, Date startDate, Date endDate);
	
}
