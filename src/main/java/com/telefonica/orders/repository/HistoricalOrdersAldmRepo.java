package com.telefonica.orders.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.orders.entity.HistoricalOrdersAldm;

@Repository
public interface HistoricalOrdersAldmRepo extends JpaRepository<HistoricalOrdersAldm, Serializable> {

	@Query("select u from HistoricalOrdersAldm u where u.phoneNumber = ?1 and u.orderDate between ?2 and ?3")
	public abstract List<HistoricalOrdersAldm> findOrdersAtisAmdocs(String publicId, Date startDate, Date endDate);

	@Query("select u from HistoricalOrdersAldm u where u.serviceTvCode = ?1 and u.orderDate between ?2 and ?3")
	public abstract List<HistoricalOrdersAldm> findOrdersCMS(String publicId, Date startDate, Date endDate);

}
