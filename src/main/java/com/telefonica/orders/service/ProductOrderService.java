package com.telefonica.orders.service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.orders.commons.Constant;
import com.telefonica.orders.commons.ErrorConstant;
import com.telefonica.orders.commons.Util;
import com.telefonica.orders.entity.HistoricalOrdersAldm;
import com.telefonica.orders.entity.OrdersAtisCms;
import com.telefonica.orders.exception.BusinessException;
import com.telefonica.orders.model.Order;
import com.telefonica.orders.pojo.response.ChannelRefType;
import com.telefonica.orders.pojo.response.KeyValueType;
import com.telefonica.orders.pojo.response.NoteInfoType;
import com.telefonica.orders.pojo.response.OrderItemType;
import com.telefonica.orders.pojo.response.OrderItemType.ActionEnum;
import com.telefonica.orders.pojo.response.OrderItemType.ActivationStatusEnum;
import com.telefonica.orders.pojo.response.ProductOrderType;
import com.telefonica.orders.pojo.response.ProductOrderType.StatusEnum;
import com.telefonica.orders.pojo.response.RelatedObjectType;
import com.telefonica.orders.pojo.response.RelatedPartyRefType;
import com.telefonica.orders.repository.HistoricalOrdersAldmRepo;
import com.telefonica.orders.repository.OrdersAtisCmsRepo;

@Service
public class ProductOrderService implements IProductOrdersService {

	@Autowired
	HistoricalOrdersAldmRepo historicalRepo;

	@Autowired
	OrdersAtisCmsRepo ordersRepo;

	public List<ProductOrderType> retrieveProductOrders(String migrationIndicator, String originSystem,
			OffsetDateTime startOrderDate, OffsetDateTime endOrderDate, String productType, String publicId) {
		if (productType == null || publicId == null || productType.isEmpty() || publicId.isEmpty()
				|| productType.trim().equals("") || publicId.trim().equals("")) {
			throw new BusinessException(ErrorConstant.SVC1000_TEXT);
		}
		Date startDate, endDate, temporalDate;
		try {

			if (startOrderDate == null && endOrderDate == null) {
				Calendar date = Calendar.getInstance();
				endDate = date.getTime();
				date.add(Calendar.MONDAY, -3);
				startDate = date.getTime();
			} else {
				endDate = Date.from(endOrderDate.toInstant());
				temporalDate = Date.from(endOrderDate.minusMonths(3).toInstant());
				startDate = Date.from(startOrderDate.toInstant());
				if (startDate.compareTo(temporalDate) < 0) {
					startDate = temporalDate;
				}
			}
		} catch (BusinessException ex) {
			throw new BusinessException(ErrorConstant.SVC1001_TEXT);
		}

		return findOrders(productType, publicId, startDate, endDate);
	}

	public List<ProductOrderType> findOrders(String productType, String publicId, Date startOrderDate,
			Date endOrderDate) {
		String type = null;
		String source = null;
		List<Order> listOrders = new ArrayList<Order>();
		List<HistoricalOrdersAldm> listHistorical = new ArrayList<HistoricalOrdersAldm>();
		List<OrdersAtisCms> listOrderAtisCms = new ArrayList<OrdersAtisCms>();
		switch (productType) {
		case Constant.MOBILE:
			// AMDOCS phoneNumber
			type = Constant.ATIS_AMDOCS;
			source = Constant.AMDOCS;
			break;
		case Constant.LANDLINE:
			// ATIS phoneNumber
			type = Constant.ATIS_AMDOCS;
			source = Constant.ATIS;
			break;
		case Constant.CABLETV:
			// CMS serviceTvCode
			type = Constant.CMS;
			source = Constant.CMS;
			break;
		case Constant.BROADBAND:
			// CMS serviceTvCode
			type = Constant.CMS;
			source = Constant.CMS;
			break;
		}
		switch (type) {
		case Constant.ATIS_AMDOCS:
			listHistorical = historicalRepo.findOrdersAtisAmdocs(publicId, startOrderDate, endOrderDate);
			listOrderAtisCms = ordersRepo.findOrdersAtisAmdocs(publicId, startOrderDate, endOrderDate);
			break;
		case Constant.CMS:
			listHistorical = historicalRepo.findOrdersCMS(publicId, startOrderDate, endOrderDate);
			listOrderAtisCms = ordersRepo.findOrdersCMS(publicId, startOrderDate, endOrderDate);
			break;
		}
		for (HistoricalOrdersAldm order : listHistorical) {
			if (order != null) {
				listOrders.add(Util.convertHistoricalToOrders(order));
			}
		}
		for (OrdersAtisCms order : listOrderAtisCms) {
			if (order != null) {
				listOrders.add(Util.convertOrderAtisCmsToOrders(order));
			}
		}
		if (listOrders.size() == 0) {
			throw new BusinessException(ErrorConstant.SVC1006_CODE);
		}
		return retrieveOrders(listOrders, productType, source, startOrderDate, endOrderDate);

	}

	public List<ProductOrderType> retrieveOrders(List<Order> listOrders, String productType, String source,
			Date startOrderDate, Date endOrderDate) {
		List<ProductOrderType> listProductOrders = new ArrayList<ProductOrderType>();
		Map<String, List<Order>> mapOrders = new HashMap<>();
		ProductOrderType productOrder;
		RelatedPartyRefType rParty1, rParty2, rParty3, rParty4;
		RelatedObjectType oType1, oType2;
		KeyValueType addData1, addData2, addData3, additionalData;
		ChannelRefType channel1, channel2, channel3;
		NoteInfoType note1, note2;
		List<RelatedPartyRefType> listRParty;
		List<RelatedObjectType> listType;
		List<KeyValueType> listAddData;
		List<ChannelRefType> listChannel;
		List<NoteInfoType> listNotes;
		List<OrderItemType> listOrderItem;
		OrderItemType orderItem;
		Order order;
		mapOrders = listOrders.stream().collect(Collectors.groupingBy(p -> p.getPhoneNumber()));
		for (Map.Entry<String, List<Order>> mapOrder : mapOrders.entrySet()) {
			order = mapOrder.getValue().get(0);
			productOrder = new ProductOrderType();
			if (order.getOrderCode() != null) {
				productOrder.setId(order.getOrderCode().toString());
			}
			productOrder.setHref(Constant.HREF_ROUTE + productOrder.getId());
			productOrder.setCorrelationId(order.getSalesRequestCode());
			productOrder.setProductOrderType(order.getOrderCategory());
			if (order.getAccountCode() != null) {
				productOrder.setAccountId(order.getAccountCode().toString());
			}
			rParty1 = new RelatedPartyRefType();
			if (order.getCustomerCode() != null) {
				rParty1.setId(order.getCustomerCode().toString());
			}
			rParty1.setRole(Constant.CUSTOMER);
			rParty2 = new RelatedPartyRefType();
			rParty2.setId(order.getUserRegisterOrder());
			rParty2.setRole(Constant.CREATION_USER);
			rParty3 = new RelatedPartyRefType();
			rParty3.setId(order.getLastChangeUser());
			rParty3.setRole(Constant.STATUS_CHANGE_USER);
			rParty4 = new RelatedPartyRefType();
			rParty4.setId(order.getPaymentUser());
			rParty4.setRole(Constant.COMPLETION_USER);
			listRParty = new ArrayList<RelatedPartyRefType>();
			listRParty.add(rParty1);
			listRParty.add(rParty2);
			listRParty.add(rParty3);
			listRParty.add(rParty4);
			productOrder.setRelatedParty(listRParty);

			oType1 = new RelatedObjectType();
			oType1.setInvolvement(productType);
			oType1.setReference(Util.findCodeService(order, productType));
			oType2 = new RelatedObjectType();
			oType2.setInvolvement(Constant.INVOLVEMENT);
			oType2.setReference(order.getSalesRequestCode());

			addData1 = new KeyValueType();
			addData1.setKey(Constant.DATE_AND_HOUR);
			if (order.getSaleDate() != null) {
				addData1.setValue(order.getSaleDate().toString());
			}
			addData2 = new KeyValueType();
			addData2.setKey(Constant.REGISTRATION_USER);
			addData2.setValue(order.getUserRegisterRequest());
			addData3 = new KeyValueType();
			addData3.setKey(Constant.REGISTRATION_CHANNEL);
			addData3.setValue(order.getChannelRegisterRequest());
			listAddData = new ArrayList<KeyValueType>();
			listAddData.add(addData1);
			listAddData.add(addData2);
			listAddData.add(addData3);
			oType2.setAdditionalData(listAddData);
			listType = new ArrayList<RelatedObjectType>();
			listType.add(oType1);
			listType.add(oType2);
			productOrder.setRelatedObject(listType);

			channel1 = new ChannelRefType();
			channel1.setId(order.getChannelRegisterOrder());
			channel1.setHref(Constant.HREF_REGISTRATION_CHANNEL + order.getChannelRegisterOrder());
			channel1.setDescription(Constant.REGISTRATION_CHANNEL);
			channel2 = new ChannelRefType();
			channel2.setId(order.getLastChangeUserChannel());
			channel2.setHref(Constant.HREF_REGISTRATION_CHANNEL + order.getLastChangeUserChannel());
			channel2.setDescription(Constant.LAST_CHANGE_CHANNEL);
			channel3 = new ChannelRefType();
			channel3.setId(order.getPaymentUserChannel());
			channel3.setHref(Constant.HREF_REGISTRATION_CHANNEL + channel3.getId());
			channel3.setDescription(Constant.SENTTLEMENT_CHANNEL);
			listChannel = new ArrayList<ChannelRefType>();
			listChannel.add(channel1);
			listChannel.add(channel2);
			listChannel.add(channel3);
			productOrder.setChannel(listChannel);
			if (order.getOrderDate() != null) {
				productOrder.setOrderDate(Util.getOffsetDateTime(order.getOrderDate()));
			}
			if (order.getPaymentDate() != null) {
				productOrder.setCompletionDate(Util.getOffsetDateTime(order.getPaymentDate()));
			}
			note1 = new NoteInfoType();
			if (order.getOrderDate() != null) {
				note1.setDate(Util.getOffsetDateTime(order.getOrderDate()));
			}

			note1.setAuthor(order.getUserRegisterOrder());
			note1.setText(order.getRegisterObservation());
			note2 = new NoteInfoType();
			if (order.getPaymentDate() != null) {
				note2.setDate(Util.getOffsetDateTime(order.getPaymentDate()));
			}
			note2.author(order.getPaymentUser());
			note2.setText(order.getPaymentObservation());
			listNotes = new ArrayList<NoteInfoType>();
			listNotes.add(note1);
			listNotes.add(note2);
			productOrder.setNote(listNotes);

			productOrder.setStatus(StatusEnum.NEW);
			productOrder.setSubStatus(order.getStatusOrder());
			if (order.getLastChangeDate() != null) {
				productOrder.setStatusChangeDate(Util.getOffsetDateTime(order.getLastChangeDate()));
			}
			productOrder.setSource(source);
			listOrderItem = new ArrayList<>();
			for (Order orderTemp : mapOrder.getValue()) {
				orderItem = new OrderItemType();
				orderItem.setDescription(orderTemp.getDetailComponentsOrder());
				orderItem.setAction(ActionEnum.UNDEFINED);
				orderItem.setQuantity(Constant.QUANTITY);
				orderItem.setActivationStatus(ActivationStatusEnum.NEW);
				orderItem.setActivationSubStatus(orderTemp.getStatusComponentsOrder());
				listOrderItem.add(orderItem);
			}
			productOrder.setOrderItem(listOrderItem);
			additionalData = new KeyValueType();
			additionalData.setKey(Constant.INSCRIPTION);
			additionalData.setValue(order.getRegistrationNumber());
			productOrder.addAdditionalDataItem(additionalData);
			listProductOrders.add(productOrder);
		}
		return listProductOrders;
	}

}
