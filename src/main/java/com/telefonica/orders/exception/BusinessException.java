package com.telefonica.orders.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BusinessException extends RuntimeException {
	private final String businessError;
	private final String codError;

	private static final long serialVersionUID = -803774844847541913L;

	public BusinessException(String codError) {
		this.businessError = "";
		this.codError = codError;
	}
	
	public BusinessException(String codError,String businessError) {
		this.businessError = businessError;
		this.codError = codError;
	}

	public BusinessException(Throwable cause, String businessError,String codError) {
		super(cause);
		this.businessError = businessError;
		this.codError = codError;
	}
}
