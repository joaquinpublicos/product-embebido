package com.telefonica.orders.exception;

import java.sql.SQLException;
import java.sql.SQLTransientConnectionException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.hibernate.exception.JDBCConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.telefonica.orders.commons.Constant;
import com.telefonica.orders.commons.ErrorConstant;
import com.telefonica.orders.commons.Util;
import com.telefonica.orders.exceptions.ExceptionType;

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(RestExceptionHandler.class);

	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1003_CODE);
		error.setExceptionText(ErrorConstant.SVC1003_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVC1003_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVC1003_TEXT);

		Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
		if (!CollectionUtils.isEmpty(supportedMethods)) {
			headers.setAllow(supportedMethods);
		}
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, headers, HttpStatus.BAD_REQUEST);

	}

	@Override
	protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1000_CODE);
		error.setExceptionText(ErrorConstant.SVC1000_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVC1000_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVC1000_TEXT.concat(ex.getVariableName()));
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, headers, HttpStatus.BAD_REQUEST);
	}

	@Override
	protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1000_CODE);
		error.setExceptionText(ErrorConstant.SVC1000_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVC1000_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVC1000_TEXT.concat(ex.getParameterName()));
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, headers, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ ConstraintViolationException.class })
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {
		List<String> errors = new ArrayList<String>();
		ex.getConstraintViolations().parallelStream()
				.forEach(cv -> errors.add(cv.getRootBeanClass().getName().concat(": ").concat(cv.getMessage())));
		StringJoiner joiner = new StringJoiner(",", "[", "]");
		errors.forEach(joiner::add);

		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1001_CODE);
		error.setExceptionText(ErrorConstant.SVC1001_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVC1001_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVC1001_TEXT.concat(joiner.toString()));
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVR1000_CODE);
		error.setExceptionText(ErrorConstant.SVR1000_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVR1000_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVR1000_TEXT);
		LOGGER.error("TrackingId: ".concat(Util.getTracking()));
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1001_CODE);
		error.setExceptionText(ErrorConstant.SVC1001_DESCRIPTION);
		error.setMoreInfo(ErrorConstant.SVC1001_EXCEPTION);
		error.setUserMessage(ErrorConstant.SVC1001_TEXT);
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(BusinessException.class)
	public final ResponseEntity<Object> handleBusinessException(BusinessException ex) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVC1006_CODE);
		error.setExceptionText(ErrorConstant.SVC1006_TEXT);
		error.setMoreInfo(ErrorConstant.SVC1006_DESCRIPTION);
		error.setUserMessage(ErrorConstant.SVC1006_EXCEPTION);
		this.printCustomError(error);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	    @Order(Ordered.HIGHEST_PRECEDENCE)
	    @ExceptionHandler({JDBCConnectionException.class, SQLException.class, SQLTransientConnectionException.class})
	    public final ResponseEntity<Object> handleJDBCConnectionException(JDBCConnectionException ex) {
		ExceptionType error = new ExceptionType();
		error.setExceptionId(ErrorConstant.SVR1008_CODE);
		error.setExceptionText(ErrorConstant.SVR1008_TEXT);
		error.setMoreInfo(ErrorConstant.SVR1008_DESCRIPTION);
		error.setUserMessage(ErrorConstant.SVR1008_EXCEPTION);
		LOGGER.error("TrackingId: ".concat(Util.getTracking()));
		LOGGER.error(ExceptionUtils.getStackTrace(ex));
		return new ResponseEntity<>(error, HttpStatus.SERVICE_UNAVAILABLE);
	    }

	private void printCustomError(ExceptionType error) {
		StringBuilder log = new StringBuilder();
		log.append("[Code Error]: ");
		log.append(error.getExceptionId());
		log.append(Constant.NEW_LINE);
		log.append("[Exception Message]: ");
		log.append(error.getMoreInfo());
		log.append(Constant.NEW_LINE);
		logError(log.toString(), null);
	}

	private static void logError(String log, Exception e) {
		StringBuilder finalLog = new StringBuilder();
		finalLog.append(getHeaderForLog());
		finalLog.append(log);
		finalLog.append(getFooterLog());
		if (e != null) {
			LOGGER.error(finalLog.toString(), e);
		} else {
			LOGGER.error(finalLog.toString());
		}
	}

	private static String getHeaderForLog() {
		StringBuilder headerLog = new StringBuilder();
		headerLog.append(Constant.NEW_LINE + Constant.SEPARATOR + Constant.NEW_LINE);
		headerLog.append("[TrackingID]: ");
		headerLog.append(Util.getTracking());
		headerLog.append(Constant.NEW_LINE);
		headerLog.append("[Date]: ");
		headerLog.append(new Date());
		headerLog.append(Constant.NEW_LINE);
		headerLog.append("[Time]: ");
		headerLog.append(Util.getDateFormat(new Date(), Constant.TIME));
		headerLog.append(Constant.NEW_LINE + "" + Constant.NEW_LINE);
		return headerLog.toString();
	}

	private static String getFooterLog() {
		return Constant.SEPARATOR;
	}

}
