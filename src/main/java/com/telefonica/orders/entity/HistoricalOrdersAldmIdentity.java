package com.telefonica.orders.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class HistoricalOrdersAldmIdentity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codComponent;

	private Integer orderCode;

	public HistoricalOrdersAldmIdentity(String codComponent, Integer orderCode) {
		super();
		this.codComponent = codComponent;
		this.orderCode = orderCode;
	}

	public HistoricalOrdersAldmIdentity() {
		super();
	}

	
	
}
