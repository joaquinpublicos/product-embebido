package com.telefonica.orders.entity;

import java.io.Serializable;

import lombok.Data;

@Data
public class OrdersAtisCmsIdentity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codComponent;

	private Integer orderCode;

	public OrdersAtisCmsIdentity(String codComponent, Integer orderCode) {
		super();
		this.codComponent = codComponent;
		this.orderCode = orderCode;
	}

	public OrdersAtisCmsIdentity() {
		super();
	}
	
	
}
