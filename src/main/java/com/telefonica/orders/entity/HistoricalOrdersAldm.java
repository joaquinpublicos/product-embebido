package com.telefonica.orders.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "HISTORICO_PEDIDO_ALDM")
@IdClass(HistoricalOrdersAldmIdentity.class)
public class HistoricalOrdersAldm implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="COD_COMPONENTE")
	private String codComponent;
	
	@Id
	@Column(name = "COD_PEDIDO")
	private Integer orderCode;

	@Column(name = "NUM_TEL")
	private String phoneNumber;

	@Column(name = "COD_CLIENTE")
	private Integer customerCode;

	@Column(name = "COD_CUENTA")
	private Integer accountCode;

	@Column(name = "NUM_INSCRIPCION")
	private String registrationNumber;

	@Column(name = "COD_SERV_TV")
	private String serviceTvCode;

	@Column(name = "COD_SOLICITUD_VENTA")
	private String salesRequestCode;

	@Column(name = "FECHA_HORA_VENTA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date saleDate;

	@Column(name = "USU_REG_SOLICITUD")
	private String userRegisterRequest;

	@Column(name = "CANAL_REG_SOLICITUD")
	public String channelRegisterRequest;

	@Column(name = "FECHA_HORA_PEDIDO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderDate;

	@Column(name = "USU_REG_PEDIDO")
	private String userRegisterOrder;

	@Column(name = "CANAL_REG_PEDIDO")
	private String channelRegisterOrder;

	@Column(name = "CAT_PEDIDO")
	private String orderCategory;

	@Column(name = "ESTADO_PEDIDO")
	private String statusOrder;

	@Column(name = "DETALLE_COMP_PEDIDO")
	private String detailComponentsOrder;

	@Column(name = "ESTADO_COMP_PEDIDO")
	private String statusComponentsOrder;

	@Column(name = "OBS_REGISTRO")
	private String registerObservation;

	@Column(name = "FECHA_HORA_ULTIMO_CAMBIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastChangeDate;

	@Column(name = "USU_ULTIMO_CAMBIO")
	private String lastChangeUser;

	@Column(name = "CANAL_USU_ULTIMO_CAMBIO")
	private String lastChangeUserChannel;

	@Column(name = "FECHA_HORA_LIQUIDACION")
	@Temporal(TemporalType.TIMESTAMP)
	private Date paymentDate;

	@Column(name = "USU_LIQUIDACION")
	private String paymentUser;

	@Column(name = "CANAL_USU_LIQUIDACION")
	private String paymentUserChannel;

	@Column(name = "OBS_LIQUIDACION")
	private String paymentObservation;

	@Column(name = "CREATION_USER")
	private String creationUser;

	@Column(name = "CREATION_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
//	@Column(name = "CREATION_IP")
//	private String creationIp;
//
//	@Column(name = "MODIFICATION_USER")
//	private String modificationUser;
//
//	@Column(name = "MODIFICATION_DATE")
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date modificationDate;
//
//	@Column(name = "MODIFICATION_IP")
//	private Date modificationIp;
}
