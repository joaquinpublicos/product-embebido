package com.telefonica.orders.pojo.response;

import java.time.OffsetDateTime;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * NoteInfoType
 */
@Validated
public class NoteInfoType {
	@JsonProperty("date")
	private OffsetDateTime date = null;

	@JsonProperty("author")
	private String author = null;

	@JsonProperty("text")
	private String text = null;

	public NoteInfoType date(OffsetDateTime date) {
		this.date = date;
		return this;
	}

	/**
	 * Date when the note was created to the order
	 * 
	 * @return date
	 **/
	@ApiModelProperty(required = true, value = "Date when the note was created to the order")
	@NotNull

	@Valid
	public OffsetDateTime getDate() {
		return date;
	}

	public void setDate(OffsetDateTime date) {
		this.date = date;
	}

	public NoteInfoType author(String author) {
		this.author = author;
		return this;
	}

	/**
	 * Identification of the originator of the note. Meaning of this is
	 * implementation and application specific, it could be, for instance, the login
	 * name of a persona that could access to read/modify the order details via a
	 * web portal
	 * 
	 * @return author
	 **/
	@ApiModelProperty(value = "Identification of the originator of the note. Meaning of this is implementation and application specific, it could be, for instance, the login name of a persona that could access to read/modify the order details via a web portal")

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public NoteInfoType text(String text) {
		this.text = text;
		return this;
	}

	/**
	 * Contents of the note (comment) to be associated to the order
	 * 
	 * @return text
	 **/
	@ApiModelProperty(required = true, value = "Contents of the note (comment) to be associated to the order")
	@NotNull

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		NoteInfoType noteInfoType = (NoteInfoType) o;
		return Objects.equals(this.date, noteInfoType.date) && Objects.equals(this.author, noteInfoType.author)
				&& Objects.equals(this.text, noteInfoType.text);
	}

	@Override
	public int hashCode() {
		return Objects.hash(date, author, text);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class NoteInfoType {\n");

		sb.append("    date: ").append(toIndentedString(date)).append("\n");
		sb.append("    author: ").append(toIndentedString(author)).append("\n");
		sb.append("    text: ").append(toIndentedString(text)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
