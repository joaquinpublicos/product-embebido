package com.telefonica.orders.pojo.response;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ProductOrderType
 */
@Validated
public class ProductOrderType {
	@JsonProperty("id")
	private String id = null;

	@JsonProperty("href")
	private String href = null;

	@JsonProperty("correlationId")
	private String correlationId = null;

	@JsonProperty("description")
	private String description = null;

	@JsonProperty("productOrderType")
	private String productOrderType = null;

	@JsonProperty("accountId")
	private String accountId = null;

	@JsonProperty("relatedParty")
	@Valid
	private List<RelatedPartyRefType> relatedParty = new ArrayList<>();

	@JsonProperty("relatedObject")
	@Valid
	private List<RelatedObjectType> relatedObject = null;

	@JsonProperty("channel")
	@Valid
	private List<ChannelRefType> channel = null;

	@JsonProperty("orderDate")
	private OffsetDateTime orderDate = null;

	@JsonProperty("completionDate")
	private OffsetDateTime completionDate = null;

	@JsonProperty("note")
	@Valid
	private List<NoteInfoType> note = null;

	/**
	 * Tracks the lifecycle status of the product order in the internal ordering
	 * state machine of the processing system
	 */
	public enum StatusEnum {
		NEW("new"),

		SUBMITTED("submitted"),

		REJECTED("rejected"),

		ACKNOWLEDGED("acknowledged"),

		IN_PROGRESS("in progress"),

		HELD("held"),

		CANCELLED("cancelled"),

		PARTIAL("partial"),

		PENDING("pending"),

		FAILED("failed"),

		COMPLETED("completed");

		private String value;

		StatusEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static StatusEnum fromValue(String text) {
			for (StatusEnum b : StatusEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("status")
	private StatusEnum status = null;

	@JsonProperty("subStatus")
	private String subStatus = null;

	@JsonProperty("statusChangeDate")
	private OffsetDateTime statusChangeDate = null;

	@JsonProperty("source")
	private String source = null;

	@JsonProperty("orderItem")
	@Valid
	private List<OrderItemType> orderItem = null;

	@JsonProperty("additionalData")
	@Valid
	private List<KeyValueType> additionalData = null;

	public ProductOrderType id(String id) {
		this.id = id;
		return this;
	}

	/**
	 * Unique Identifier within the server for the order reported
	 * 
	 * @return id
	 **/
	@ApiModelProperty(required = true, value = "Unique Identifier within the server for the order reported")
	@NotNull

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ProductOrderType href(String href) {
		this.href = href;
		return this;
	}

	/**
	 * A resource URI pointing to the resource in the OB that stores the order
	 * detailed information
	 * 
	 * @return href
	 **/
	@ApiModelProperty(required = true, value = "A resource URI pointing to the resource in the OB that stores the order detailed information")
	@NotNull

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public ProductOrderType correlationId(String correlationId) {
		this.correlationId = correlationId;
		return this;
	}

	/**
	 * Unique identifier for the order created within the client, used to
	 * synchronize and map internal identifiers between server and client. Notice
	 * that in the TMForum API version 16.5 this parameter is named externalId
	 * 
	 * @return correlationId
	 **/
	@ApiModelProperty(value = "Unique identifier for the order created within the client, used to synchronize and map internal identifiers between server and client. Notice that in the TMForum API version 16.5 this parameter is named externalId")

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public ProductOrderType description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Some text providing a user-friendly detailed description of the product order
	 * 
	 * @return description
	 **/
	@ApiModelProperty(value = "Some text providing a user-friendly detailed description of the product order")

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ProductOrderType productOrderType(String productOrderType) {
		this.productOrderType = productOrderType;
		return this;
	}

	/**
	 * Indication of the type of order registered. Supported values are
	 * implementation and application specific. Used to categorize the order from a
	 * business perspective that can be useful for the OM system (e.g.: enterprise,
	 * residential, ...). Notice that in the TMForum API version 16.5 this parameter
	 * is named category
	 * 
	 * @return productOrderType
	 **/
	@ApiModelProperty(value = "Indication of the type of order registered. Supported values are implementation and application specific. Used to categorize the order from a business perspective that can be useful for the OM system (e.g.: enterprise, residential, ...). Notice that in the TMForum API version 16.5 this parameter is named category")

	public String getProductOrderType() {
		return productOrderType;
	}

	public void setProductOrderType(String productOrderType) {
		this.productOrderType = productOrderType;
	}

	public ProductOrderType accountId(String accountId) {
		this.accountId = accountId;
		return this;
	}

	/**
	 * Unique identifier for the account within the server to be linked to the order
	 * (e.g.: customer account number)
	 * 
	 * @return accountId
	 **/
	@ApiModelProperty(value = "Unique identifier for the account within the server to be linked to the order (e.g.: customer account number)")

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public ProductOrderType relatedParty(List<RelatedPartyRefType> relatedParty) {
		this.relatedParty = relatedParty;
		return this;
	}

	public ProductOrderType addRelatedPartyItem(RelatedPartyRefType relatedPartyItem) {
		this.relatedParty.add(relatedPartyItem);
		return this;
	}

	/**
	 * List of individuals (e.g.: support agent, system impacted, reviewer, u
	 * associated to the order
	 * 
	 * @return relatedParty
	 **/
	@ApiModelProperty(required = true, value = "List of individuals (e.g.: support agent, system impacted, reviewer, u associated to the order")
	@NotNull
	@Valid
	public List<RelatedPartyRefType> getRelatedParty() {
		return relatedParty;
	}

	public void setRelatedParty(List<RelatedPartyRefType> relatedParty) {
		this.relatedParty = relatedParty;
	}

	public ProductOrderType relatedObject(List<RelatedObjectType> relatedObject) {
		this.relatedObject = relatedObject;
		return this;
	}

	public ProductOrderType addRelatedObjectItem(RelatedObjectType relatedObjectItem) {
		if (this.relatedObject == null) {
			this.relatedObject = new ArrayList<RelatedObjectType>();
		}
		this.relatedObject.add(relatedObjectItem);
		return this;
	}

	/**
	 * List of Objects or resources associated to an order (e.g.: framework
	 * agreement, opportunity, u
	 * 
	 * @return relatedObject
	 **/
	@ApiModelProperty(value = "List of Objects or resources associated to an order (e.g.: framework agreement, opportunity, u")
	@Valid
	public List<RelatedObjectType> getRelatedObject() {
		return relatedObject;
	}

	public void setRelatedObject(List<RelatedObjectType> relatedObject) {
		this.relatedObject = relatedObject;
	}

	public ProductOrderType channel(List<ChannelRefType> channel) {
		this.channel = channel;
		return this;
	}

	public ProductOrderType addChannelItem(ChannelRefType channelItem) {
		if (this.channel == null) {
			this.channel = new ArrayList<ChannelRefType>();
		}
		this.channel.add(channelItem);
		return this;
	}

	/**
	 * Defines the channel that is used to trigger the order. This element is
	 * defined as an array in order to allow the option to associate an order to
	 * multiple channels
	 * 
	 * @return channel
	 **/
	@ApiModelProperty(value = "Defines the channel that is used to trigger the order. This element is defined as an array in order to allow the option to associate an order to multiple channels")
	@Valid
	public List<ChannelRefType> getChannel() {
		return channel;
	}

	public void setChannel(List<ChannelRefType> channel) {
		this.channel = channel;
	}

	public ProductOrderType orderDate(OffsetDateTime orderDate) {
		this.orderDate = orderDate;
		return this;
	}

	/**
	 * Date when the order was created in the server
	 * 
	 * @return orderDate
	 **/
	@ApiModelProperty(required = true, value = "Date when the order was created in the server")
	@NotNull

	@Valid
	public OffsetDateTime getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(OffsetDateTime orderDate) {
		this.orderDate = orderDate;
	}

	public ProductOrderType completionDate(OffsetDateTime completionDate) {
		this.completionDate = completionDate;
		return this;
	}

	/**
	 * Actual date when delivery of all the items within an order was complete. This
	 * could be required in order to manage SLA compliancy
	 * 
	 * @return completionDate
	 **/
	@ApiModelProperty(value = "Actual date when delivery of all the items within an order was complete. This could be required in order to manage SLA compliancy")

	@Valid
	public OffsetDateTime getCompletionDate() {
		return completionDate;
	}

	public void setCompletionDate(OffsetDateTime completionDate) {
		this.completionDate = completionDate;
	}

	public ProductOrderType note(List<NoteInfoType> note) {
		this.note = note;
		return this;
	}

	public ProductOrderType addNoteItem(NoteInfoType noteItem) {
		if (this.note == null) {
			this.note = new ArrayList<NoteInfoType>();
		}
		this.note.add(noteItem);
		return this;
	}

	/**
	 * List of notes added as part of the order
	 * 
	 * @return note
	 **/
	@ApiModelProperty(value = "List of notes added as part of the order")
	@Valid
	public List<NoteInfoType> getNote() {
		return note;
	}

	public void setNote(List<NoteInfoType> note) {
		this.note = note;
	}

	public ProductOrderType status(StatusEnum status) {
		this.status = status;
		return this;
	}

	/**
	 * Tracks the lifecycle status of the product order in the internal ordering
	 * state machine of the processing system
	 * 
	 * @return status
	 **/
	@ApiModelProperty(required = true, value = "Tracks the lifecycle status of the product order in the internal ordering state machine of the processing system")
	@NotNull

	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}

	public ProductOrderType subStatus(String subStatus) {
		this.subStatus = subStatus;
		return this;
	}

	/**
	 * Substatus in order to define a second status level
	 * 
	 * @return subStatus
	 **/
	@ApiModelProperty(value = "Substatus in order to define a second status level")

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public ProductOrderType statusChangeDate(OffsetDateTime statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
		return this;
	}

	/**
	 * Date registered for the last change of status
	 * 
	 * @return statusChangeDate
	 **/
	@ApiModelProperty(value = "Date registered for the last change of status")

	@Valid
	public OffsetDateTime getStatusChangeDate() {
		return statusChangeDate;
	}

	public void setStatusChangeDate(OffsetDateTime statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}

	public ProductOrderType source(String source) {
		this.source = source;
		return this;
	}

	/**
	 * Source that contains the order
	 * 
	 * @return source
	 **/
	@ApiModelProperty(value = "Source that contains the order")

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public ProductOrderType orderItem(List<OrderItemType> orderItem) {
		this.orderItem = orderItem;
		return this;
	}

	public ProductOrderType addOrderItemItem(OrderItemType orderItemItem) {
		if (this.orderItem == null) {
			this.orderItem = new ArrayList<OrderItemType>();
		}
		this.orderItem.add(orderItemItem);
		return this;
	}

	/**
	 * List of individual items included in the order
	 * 
	 * @return orderItem
	 **/
	@ApiModelProperty(value = "List of individual items included in the order")
	@Valid
	@Size(min = 1)
	public List<OrderItemType> getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(List<OrderItemType> orderItem) {
		this.orderItem = orderItem;
	}

	public ProductOrderType additionalData(List<KeyValueType> additionalData) {
		this.additionalData = additionalData;
		return this;
	}

	public ProductOrderType addAdditionalDataItem(KeyValueType additionalDataItem) {
		if (this.additionalData == null) {
			this.additionalData = new ArrayList<KeyValueType>();
		}
		this.additionalData.add(additionalDataItem);
		return this;
	}

	/**
	 * Any further information needed by the server to fill the entity definition.
	 * It is recommended not to use this parameter and request new information
	 * elements to be added in the structure definition. Next releases of the T-Open
	 * API will not include support for this additionalData parameter because it has
	 * been detected that the extensibility function is not helping the stability of
	 * the standard definition of APIs
	 * 
	 * @return additionalData
	 **/
	@ApiModelProperty(value = "Any further information needed by the server to fill the entity definition. It is recommended not to use this parameter and request new information elements to be added in the structure definition. Next releases of the T-Open API will not include support for this additionalData parameter because it has been detected that the extensibility function is not helping the stability of the standard definition of APIs")
	@Valid
	public List<KeyValueType> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(List<KeyValueType> additionalData) {
		this.additionalData = additionalData;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		ProductOrderType productOrderType = (ProductOrderType) o;
		return Objects.equals(this.id, productOrderType.id) && Objects.equals(this.href, productOrderType.href)
				&& Objects.equals(this.correlationId, productOrderType.correlationId)
				&& Objects.equals(this.description, productOrderType.description)
				&& Objects.equals(this.productOrderType, productOrderType.productOrderType)
				&& Objects.equals(this.accountId, productOrderType.accountId)
				&& Objects.equals(this.relatedParty, productOrderType.relatedParty)
				&& Objects.equals(this.relatedObject, productOrderType.relatedObject)
				&& Objects.equals(this.channel, productOrderType.channel)
				&& Objects.equals(this.orderDate, productOrderType.orderDate)
				&& Objects.equals(this.completionDate, productOrderType.completionDate)
				&& Objects.equals(this.note, productOrderType.note)
				&& Objects.equals(this.status, productOrderType.status)
				&& Objects.equals(this.subStatus, productOrderType.subStatus)
				&& Objects.equals(this.statusChangeDate, productOrderType.statusChangeDate)
				&& Objects.equals(this.source, productOrderType.source)
				&& Objects.equals(this.orderItem, productOrderType.orderItem)
				&& Objects.equals(this.additionalData, productOrderType.additionalData);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, href, correlationId, description, productOrderType, accountId, relatedParty,
				relatedObject, channel, orderDate, completionDate, note, status, subStatus, statusChangeDate, source,
				orderItem, additionalData);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class ProductOrderType {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    href: ").append(toIndentedString(href)).append("\n");
		sb.append("    correlationId: ").append(toIndentedString(correlationId)).append("\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    productOrderType: ").append(toIndentedString(productOrderType)).append("\n");
		sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
		sb.append("    relatedParty: ").append(toIndentedString(relatedParty)).append("\n");
		sb.append("    relatedObject: ").append(toIndentedString(relatedObject)).append("\n");
		sb.append("    channel: ").append(toIndentedString(channel)).append("\n");
		sb.append("    orderDate: ").append(toIndentedString(orderDate)).append("\n");
		sb.append("    completionDate: ").append(toIndentedString(completionDate)).append("\n");
		sb.append("    note: ").append(toIndentedString(note)).append("\n");
		sb.append("    status: ").append(toIndentedString(status)).append("\n");
		sb.append("    subStatus: ").append(toIndentedString(subStatus)).append("\n");
		sb.append("    statusChangeDate: ").append(toIndentedString(statusChangeDate)).append("\n");
		sb.append("    source: ").append(toIndentedString(source)).append("\n");
		sb.append("    orderItem: ").append(toIndentedString(orderItem)).append("\n");
		sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
