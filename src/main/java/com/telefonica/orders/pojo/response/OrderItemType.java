package com.telefonica.orders.pojo.response;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.*;

/**
 * OrderItemType
 */
@Validated
public class OrderItemType {

	@JsonProperty("description")
	private String description = null;

	/**
	 * Indicates the order-related action requested as part of the order item (add a
	 * new product to an account, modify an existing product or remove a product
	 * from an account)
	 */
	public enum ActionEnum {
		ADD("add"),

		MODIFY("modify"),

		DELETE("delete"),

		NO_CHANGE("no_change"),

		IMPLICIT("implicit"),

		UNDEFINED("undefined");

		private String value;

		ActionEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static ActionEnum fromValue(String text) {
			for (ActionEnum b : ActionEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("action")
	private ActionEnum action = null;

	@JsonProperty("quantity")
	private String quantity = null;

	/**
	 * The status to which the product is set (for responses) or has to be set (for
	 * requests)
	 */
	public enum ActivationStatusEnum {
		NEW("new"),

		CREATED("created"),

		ACTIVE("active"),

		ABORTED("aborted"),

		SUSPENDED("suspended"),

		CANCELLED("cancelled"),

		TERMINATED("terminated"),

		PENDING("pending"),

		INFORMATIONAL("informational"),

		TRIAL("trial"),

		KEEP("keep");

		private String value;

		ActivationStatusEnum(String value) {
			this.value = value;
		}

		@Override
		@JsonValue
		public String toString() {
			return String.valueOf(value);
		}

		@JsonCreator
		public static ActivationStatusEnum fromValue(String text) {
			for (ActivationStatusEnum b : ActivationStatusEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}

	@JsonProperty("activationStatus")
	private ActivationStatusEnum activationStatus = null;

	@JsonProperty("activationSubStatus")
	private String activationSubStatus = null;

	public OrderItemType description(String description) {
		this.description = description;
		return this;
	}

	/**
	 * Some text providing a user-friendly detailed description of the product order
	 * item
	 * 
	 * @return description
	 **/
	@ApiModelProperty(value = "Some text providing a user-friendly detailed description of the product order item")

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public OrderItemType action(ActionEnum action) {
		this.action = action;
		return this;
	}

	/**
	 * Indicates the order-related action requested as part of the order item (add a
	 * new product to an account, modify an existing product or remove a product
	 * from an account)
	 * 
	 * @return action
	 **/
	@ApiModelProperty(required = true, value = "Indicates the order-related action requested as part of the order item (add a new product to an account, modify an existing product or remove a product from an account)")
	@NotNull

	public ActionEnum getAction() {
		return action;
	}

	public void setAction(ActionEnum action) {
		this.action = action;
	}

	public OrderItemType quantity(String quantity) {
		this.quantity = quantity;
		return this;
	}

	/**
	 * Number of units ordered of a given product (e.g.: number of instances to be
	 * created)
	 * 
	 * @return quantity
	 **/
	@ApiModelProperty(required = true, value = "Number of units ordered of a given product (e.g.: number of instances to be created)")
	@NotNull

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public OrderItemType activationStatus(ActivationStatusEnum activationStatus) {
		this.activationStatus = activationStatus;
		return this;
	}

	/**
	 * The status to which the product is set (for responses) or has to be set (for
	 * requests)
	 * 
	 * @return activationStatus
	 **/
	@ApiModelProperty(value = "The status to which the product is set (for responses) or has to be set (for requests)")

	public ActivationStatusEnum getActivationStatus() {
		return activationStatus;
	}

	public void setActivationStatus(ActivationStatusEnum activationStatus) {
		this.activationStatus = activationStatus;
	}

	public OrderItemType activationSubStatus(String activationSubStatus) {
		this.activationSubStatus = activationSubStatus;
		return this;
	}

	/**
	 * Substatus in order to define a second status level
	 * 
	 * @return activationSubStatus
	 **/
	@ApiModelProperty(value = "Substatus in order to define a second status level")

	public String getActivationSubStatus() {
		return activationSubStatus;
	}

	public void setActivationSubStatus(String activationSubStatus) {
		this.activationSubStatus = activationSubStatus;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrderItemType orderItemType = (OrderItemType) o;
		return Objects.equals(this.description, orderItemType.description)
				&& Objects.equals(this.action, orderItemType.action)
				&& Objects.equals(this.quantity, orderItemType.quantity)
				&& Objects.equals(this.activationStatus, orderItemType.activationStatus)
				&& Objects.equals(this.activationSubStatus, orderItemType.activationSubStatus);
	}

	@Override
	public int hashCode() {
		return Objects.hash(description, action, quantity, activationStatus, activationSubStatus);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OrderItemType {\n");
		sb.append("    description: ").append(toIndentedString(description)).append("\n");
		sb.append("    action: ").append(toIndentedString(action)).append("\n");
		sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
		sb.append("    activationStatus: ").append(toIndentedString(activationStatus)).append("\n");
		sb.append("    activationSubStatus: ").append(toIndentedString(activationSubStatus)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
