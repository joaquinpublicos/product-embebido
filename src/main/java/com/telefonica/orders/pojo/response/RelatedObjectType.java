package com.telefonica.orders.pojo.response;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * RelatedObjectType
 */
@Validated
public class RelatedObjectType {
	@JsonProperty("involvement")
	private String involvement = null;

	@JsonProperty("reference")
	private String reference = null;

	@JsonProperty("additionalData")
	@Valid
	private List<KeyValueType> additionalData = null;

	public RelatedObjectType involvement(String involvement) {
		this.involvement = involvement;
		return this;
	}

	/**
	 * Indication of the relationship defined between the object and the product
	 * order reported. Type of related object. Supported values are implementation
	 * and application specific
	 * 
	 * @return involvement
	 **/
	@ApiModelProperty(value = "Indication of the relationship defined between the object and the product order reported. Type of related object. Supported values are implementation and application specific")

	public String getInvolvement() {
		return involvement;
	}

	public void setInvolvement(String involvement) {
		this.involvement = involvement;
	}

	public RelatedObjectType reference(String reference) {
		this.reference = reference;
		return this;
	}

	/**
	 * String providing identification of the related object reported (an id or a
	 * name)
	 * 
	 * @return reference
	 **/
	@ApiModelProperty(required = true, value = "String providing identification of the related object reported (an id or a name)")
	@NotNull

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public RelatedObjectType additionalData(List<KeyValueType> additionalData) {
		this.additionalData = additionalData;
		return this;
	}

	public RelatedObjectType addAdditionalDataItem(KeyValueType additionalDataItem) {
		if (this.additionalData == null) {
			this.additionalData = new ArrayList<KeyValueType>();
		}
		this.additionalData.add(additionalDataItem);
		return this;
	}

	/**
	 * Any further information needed by the server to fill the entity definition.
	 * It is recommended not to use this parameter and request new information
	 * elements to be added in the structure definition. Next releases of the T-Open
	 * API will not include support for this additionalData parameter because it has
	 * been detected that the extensibility function is not helping the stability of
	 * the standard definition of APIs
	 * 
	 * @return additionalData
	 **/
	@ApiModelProperty(value = "Any further information needed by the server to fill the entity definition. It is recommended not to use this parameter and request new information elements to be added in the structure definition. Next releases of the T-Open API will not include support for this additionalData parameter because it has been detected that the extensibility function is not helping the stability of the standard definition of APIs")
	@Valid
	public List<KeyValueType> getAdditionalData() {
		return additionalData;
	}

	public void setAdditionalData(List<KeyValueType> additionalData) {
		this.additionalData = additionalData;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		RelatedObjectType relatedObjectType = (RelatedObjectType) o;
		return Objects.equals(this.involvement, relatedObjectType.involvement)
				&& Objects.equals(this.reference, relatedObjectType.reference)
				&& Objects.equals(this.additionalData, relatedObjectType.additionalData);
	}

	@Override
	public int hashCode() {
		return Objects.hash(involvement, reference, additionalData);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class RelatedObjectType {\n");

		sb.append("    involvement: ").append(toIndentedString(involvement)).append("\n");
		sb.append("    reference: ").append(toIndentedString(reference)).append("\n");
		sb.append("    additionalData: ").append(toIndentedString(additionalData)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
