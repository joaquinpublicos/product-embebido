package com.telefonica.orders.model;

import java.util.Date;

import lombok.Data;

@Data
public class Order {
	private Integer orderCode;

	private String phoneNumber;

	private Integer customerCode;

	private Integer accountCode;

	private String registrationNumber;

	private String serviceTvCode;

	private String salesRequestCode;

	private Date saleDate;

	private String userRegisterRequest;

	public String channelRegisterRequest;

	private Date orderDate;

	private String userRegisterOrder;

	private String channelRegisterOrder;

	private String orderCategory;

	private String statusOrder;

	private String detailComponentsOrder;

	private String statusComponentsOrder;

	private String registerObservation;

	private Date lastChangeDate;

	private String lastChangeUser;

	private String lastChangeUserChannel;

	private Date paymentDate;

	private String paymentUser;

	private String paymentUserChannel;

	private String paymentObservation;

	private String creationUser;

	private Date creationDate;
}
