package com.telefonica.orders.rest;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.orders.exceptions.ExceptionType;
import com.telefonica.orders.pojo.response.ProductOrderType;
import com.telefonica.orders.repository.HistoricalOrdersAldmRepo;
import com.telefonica.orders.repository.OrdersAtisCmsRepo;
import com.telefonica.orders.service.ProductOrderService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ResponseHeader;

@RestController
public class ProductOrdersRest {

	@Autowired
	ProductOrderService productService;

	@ApiOperation(value = "Retrieve a list of product orders", nickname = "retrieveProductOrders", notes = "", response = ProductOrderType.class, responseContainer = "List", tags = {
			"product orders", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Product orders retrieved successfully", response = ProductOrderType.class, responseContainer = "List", responseHeaders = {
					@ResponseHeader(name = "UNICA-ServiceId", response = String.class, description = "The value of this element must be the same as that indicated by the element with the same name in the header received from the service provider. It will be used to be able to correlate the messages at execution level."),
					@ResponseHeader(name = "UNICA-PID", response = String.class, description = "The value of this element must be the same as that indicated by the element with the same name in the header received from the service provider. It will be used to be able to correlate the messages.") }),
			@ApiResponse(code = 400, message = "Generic Client Error", response = ExceptionType.class),
			@ApiResponse(code = 400, message = "Invalid requested Operation", response = ExceptionType.class),
			@ApiResponse(code = 400, message = "Missing mandatory parameter", response = ExceptionType.class),
			@ApiResponse(code = 400, message = "Invalid parameter", response = ExceptionType.class),
			@ApiResponse(code = 404, message = "Not existing Resource Id", response = ExceptionType.class),
			@ApiResponse(code = 500, message = "Generic Server Fault", response = ExceptionType.class),
			@ApiResponse(code = 503, message = "Timeout processing request", response = ExceptionType.class) })
	@GetMapping(value = "/productOrders", produces = { "application/json" })
	public ResponseEntity<List<ProductOrderType>> retrieveProductOrders(
			@ApiParam(value = "If this API is used via a platform acting as a common entry point to different OBs, this identifier is used to route the request to the corresponding OB environment") @RequestHeader(value = "UNICA-ServiceId", required = false) String unICAServiceId,
			@ApiParam(value = "Identifier for the system originating the request") @RequestHeader(value = "UNICA-Application", required = false) String unICAApplication,
			@ApiParam(value = "Unique identifier for the process or execution flow") @RequestHeader(value = "UNICA-PID", required = false) String UNICA_PID,
			@ApiParam(value = "Identifies the user when the request is received from a trusted application and no end user authorization token is used but just an application token") @RequestHeader(value = "UNICA-User", required = false) String unICAUser,
			@ApiParam(value = "Including the proof of access (using OAuth2.0 security model) to guarantee that the consumer has privileges to access the entity database") @RequestHeader(value = "Authorization", required = false) String authorization,
			@ApiParam(value = "Migration indicator.") @RequestHeader(value = "migrationIndicator", required = false) String migrationIndicator,
			@ApiParam(value = "Origin system.") @RequestHeader(value = "originSystem", required = false) String originSystem,
			@ApiParam(value = "To obtain the list of orders in server that were created after this value") @Valid @RequestParam(value = "startOrderDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) OffsetDateTime startOrderDate,
			@ApiParam(value = "To obtain the list of orders in server that were created before this value") @Valid @RequestParam(value = "endOrderDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)  OffsetDateTime endOrderDate,
			@ApiParam(value = "To obtain the list of accounts matching to a given productType value", allowableValues = "mobile, landline, ipTv, cableTv, dth, email, broadband, bundle, sva, sim, device, bolton, streamingTv") @Valid @RequestParam(value = "productType", required = true) String productType,
			@ApiParam(value = "To obtain the list of accounts matching to a given product public identifier") @Valid @RequestParam(value = "publicId", required = true) String publicId) {

			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.add("UNICA-ServiceId", unICAServiceId);
			httpHeaders.add("UNICA-PID", UNICA_PID);
			List<ProductOrderType> listProducts = productService.retrieveProductOrders(migrationIndicator, originSystem,
					startOrderDate, endOrderDate, productType, publicId);
			return new ResponseEntity<List<ProductOrderType>>(listProducts, httpHeaders, HttpStatus.OK);

	}

}
